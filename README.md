# README #

Este repositorio refere-se ao Keycloak, que é usado como sistema de gestão de utilizadores.
Informação sobre o seu setup em um servidor UBUNTO pode ser vista no ficheiro keycloak.txt.


## Setup

### 1) Instalar Java e KeyCloak:   
https://medium.com/@hasnat.saeed/setup-keycloak-server-on-ubuntu-18-04-ed8c7c79a2d9

```
cd /opt    
sudo wget https://github.com/keycloak/keycloak/releases/download/17.0.0/keycloak-17.0.0.tar.gz
sudo tar -xvzf keycloak-17.0.0.tar.gz

sudo mv keycloak-17.0.0 /opt/keycloak
```

### 2) Criar o User ADMIN:

https://www.keycloak.org/docs/latest/server_admin/
```
export KEYCLOAK_ADMIN=admin
export KEYCLOAK_ADMIN_PASSWORD=<password>
printenv
sudo KEYCLOAK_ADMIN=$KEYCLOAK_ADMIN KEYCLOAK_ADMIN_PASSWORD=$KEYCLOAK_ADMIN_PASSWORD ./kc.sh start-dev
```
### 3) INSTALAR O CERTIFICADO HTTPS:
https://certbot.eff.org/instructions?ws=other&os=ubuntubionic

Seguir o site acima, e instalar um certificado para o dominio do servidor.

NOTA: Não posso copiar os certificados que ele assim nao funciona no Keycloak, se não tiverem no diretorio original.



### 4) LEVANTAR EM HTTPS:

```
sudo ./kc.sh start-dev --https-certificate-file=/etc/letsencrypt/live/gonogoapp.org/fullchain.pem --https-certificate-key-file=/etc/letsencrypt/live/gonogoapp.org/privkey.pem

ou

sudo ./kc.sh build

sudo ./kc.sh start --hostname gonogoapp.org:8443 --https-certificate-file=/etc/letsencrypt/live/gonogoapp.org/fullchain.pem --https-certificate-key-file=/etc/letsencrypt/live/gonogoapp.org/privkey.pem
```

LOGIN: https://gonogoapp.org:8443/admin/


### 5) CRIAR REALM gonogoapp
Criar o realm "gonogoapp" e alterar as seguintes opções:     

Realm gonogoapp > Login     
User Registration ON      
Login with email OFF     

### 6) CRIAR CLIENTE gonogo

Criar o cliente "gonogo" no realm criado anteriormente e fazer as seguintes configurações:     
Cliente gono > settings >     
Web Origins - adicionar * por causa do CORS
Valid Redirect URIs https://gonogapp.org

Ver a imagem CORS option.jpg para referencia.        
![CORS](CORS_option.JPG "CORS")



Cliente gono > settings > Advanced Settings           
Colocar 30 min Token livespan e o resto a 1 hora.            

Adicionar os seguintes user atributes:                   
user.attributes.birthdate          
user.attributes.gender              
user.attributes.lingua             
user.attributes.rgpd            
Cliente gono > Mappers > Create                        
Name: lingua               
Mapper Type: User Attribute               
User Attribute: lingua               
Token Clain Name: lingua                
Claim JSON Type: string                
Add to ID token: ON                 
Add to access token: ON                
Add to userinfo: ON                

Basicamente, todos estes atributos depois necessitam de estar presentes no Token de autorização que vem em base64. Dentro do campo access_token. Exemplo de um token completo:
```
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJCb2t0T0h0WmI5S0ZYSjJ0aXhiSDhLOGJ6OXhKLU9CSVhYa2t5YUo1ZE9FIn0.eyJleHAiOjE2NTQ2MjMzOTQsImlhdCI6MTY1NDYyMTU5NCwianRpIjoiZmM4NGQwYmYtYzRlYS00MTJkLTk1ODktZjgwMjBiZDY4YmZhIiwiaXNzIjoiaHR0cHM6Ly9nb25vZ29hcHAub3JnOjg0NDMvcmVhbG1zL2dvbm9nb2FwcCIsInN1YiI6IjE4MTg2ODljLTBmMjgtNDQ2My05Zjk3LTZiNGJhOTRmZWI2ZCIsInR5cCI6IkJlYXJlciIsImF6cCI6Imdvbm9nbyIsInNlc3Npb25fc3RhdGUiOiI5YjM3MzU5ZS00N2U5LTQxY2QtYWRiZi0yNTJiNDkyYTZjY2IiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwic2NvcGUiOiJwcm9maWxlIiwic2lkIjoiOWIzNzM1OWUtNDdlOS00MWNkLWFkYmYtMjUyYjQ5MmE2Y2NiIiwiYmlydGhkYXRlIjoiMTk5OS0wMi0xNiIsImdlbmRlciI6ImZlbWFsZSIsImxpbmd1YSI6InB0IiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYTEiLCJsb2NhbGUiOiJwdC1CUiIsInJncGQiOiJvbiJ9.UqrR9f_UVxAbEi5l1rFVxGwGFDG48eM8F7VzFBQmgmATCg69xZkerAaUq-casTriXKb6gSO5mS-IUt2H9irNAHz-xC5FMZ5N2sBRzMZlR55ThaCYJBCWTOs6C_LWfHPzcwVIJ-Z9LQT4BdMPH8M4R4i6tlu6lbRJIzFMLE_FYuafTrMzxQQxXjH8TPxd9mBOX8NbRqunJT-2suSQst8mGTGrkxdh5vKthlQbUahCNkhcY9RK7PoHVlqagO5AVro8p7paQaNUhZq7yyPQikNnEm1vm1n1xOPcGqxR7ZR92cgDtjlKMrxo780WdAmFUKmeUn6g-nqoVXdDPa8iO4-BKA",
    "expires_in": 1800,
    "refresh_expires_in": 1800,
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIwYjQ2NjM5OC1mODgxLTQ0ODMtYmNlMi1lMTdhOTRjMzBlMGUifQ.eyJleHAiOjE2NTQ2MjMzOTQsImlhdCI6MTY1NDYyMTU5NCwianRpIjoiMGNlMGNlZTYtMjFmMi00YTVjLWFkNGMtZTE2YTZkY2VmMDVmIiwiaXNzIjoiaHR0cHM6Ly9nb25vZ29hcHAub3JnOjg0NDMvcmVhbG1zL2dvbm9nb2FwcCIsImF1ZCI6Imh0dHBzOi8vZ29ub2dvYXBwLm9yZzo4NDQzL3JlYWxtcy9nb25vZ29hcHAiLCJzdWIiOiIxODE4Njg5Yy0wZjI4LTQ0NjMtOWY5Ny02YjRiYTk0ZmViNmQiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiZ29ub2dvIiwic2Vzc2lvbl9zdGF0ZSI6IjliMzczNTllLTQ3ZTktNDFjZC1hZGJmLTI1MmI0OTJhNmNjYiIsInNjb3BlIjoicHJvZmlsZSIsInNpZCI6IjliMzczNTllLTQ3ZTktNDFjZC1hZGJmLTI1MmI0OTJhNmNjYiJ9.deigwUWeG_r0EXy2mP9tShb_uj8-1dtr_U3Yc5AS3Bo",
    "token_type": "Bearer",
    "not-before-policy": 0,
    "session_state": "9b37359e-47e9-41cd-adbf-252b492a6ccb",
    "scope": "profile"
}
```

Para confirmar, basta autenticar um utilizador por POST em https://gonogoapp.org:8443/realms/gonogoapp/protocol/openid-connect/token e depois descodificar a secção no website https://www.base64decode.org/ . Em alternativa os testes de Postman no repositorio Backend permitem confirmar esta situação, para o user "a1" de password "a" (tem de ser criado manualmente).


### 7) Copiar o tema para o keycloak.
```
sudo cp -R /opt/fadiga/keycloak/themes/gotheme /opt/keycloak/themes/gotheme
cd /opt/keycloak/themes/
ls
sudo chmod ugo+rwx /opt/keycloak/themes/
```

## Serviço SystemD

Dentro do diretorio Servico encontra-se uma copia do servico a usar para levantar automaticamente o Keycloak no Ubunto.


